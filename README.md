
ruby_sinatra

This project is a starting point for making jruby sinatra applications

Create a base web application, using  **Warbler** to create a single jar file deploy with **JRuby**, **Sinara** and **Puma**

### Setup

The bin/puma file was created with this command:

    jruby -S bundle install --binstubs

Leave only the bin/puma in the bin directory for warbler can use it as the default executible. The .binstubs folder should be removed to the correct bin file is found when creating the jar.

I had to remove a line from the bin/puma file to get it to work with jruby:

    require 'bundler/setup'

It was causing errors like:

    Your Ruby version is 1.9.3, but your Gemfile specified 2.2.2


### Running

Update gems:

    jruby -S bundle install

Run locally:

    jruby -S rake

Deploy to remote server:

    jruby -S rake deploy

### Notes
Jruby requires the following line in the Gemfile for jruby version 9.0.0.0:

    ruby '2.2.2', :engine => 'jruby', :engine_version => '9.0.0.0'

if the version doesn't match, you will get errors like these:

    Your Ruby version is 2.2.2, but your Gemfile specified 1.9.3
    Your jruby version is 9.0.0.0, but your Gemfile specified jruby 1.7.4
